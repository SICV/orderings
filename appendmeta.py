import json

from argparse import ArgumentParser
p = ArgumentParser()
p.add_argument("input", nargs="*")
args = p.parse_args()

out = []

for n in args.input:
	with open(n) as f:
		data = json.load(f)
		for item in data:
			out.append(item)

print json.dumps(out)
