from argparse import ArgumentParser
import sys, json, os

p = ArgumentParser()
p.add_argument("input", nargs="*")
args = p.parse_args()

data = {}
items = []
data['items'] = items

for i in args.input:
	item = {'original': i}
	base = os.path.basename(i)
	item['base'] = base
	item['thumb'] = os.path.join("thumbs", base+".thumb.png")
	item['preview'] = os.path.join("previews", base+".preview.png")
	items.append(item)

print json.dumps(data)

