/*
	extend to two dimensions (again need to intersect overlapping sets!)
	generic map overview + zooming?
	layers ?!

make saveable documents...
so "reveal" events should be generic...
but i want zooming as well



"bcr" DOMRect { x: 50, y: 50, width: 840, height: 2982, top: 50, right: 890, bottom: 3032, left: 50 }
"viewport" Object { x: 0, y: 0, w: 500, h: 500 }
=> x: 0, y: 0, w: 450, h: 450

intersect bcr with viewport & translate into bcr coords...

*/

// (translate (intersect bcr, viewport), (bcr.x, bcr.y))

function rect_intersect (a, b) {
	var x = Math.max(a.x, b.x),
		num1 = Math.min(a.x + a.width, b.x + b.width),
		y = Math.max(a.y, b.y),
		num2 = Math.min(a.y + a.height, b.y + b.height);
	if (num1 >= x && num2 >= y) {
		ret = {x: x, y: y, width: num1 - x, height: num2 - y};
		return ret;  	
	} else {
		return {x: 0, y: 0, width: 0, height: 0};
	};
}

function visible_rect (elt) {
	// WANT: rectangle in elements own coordinate system that is in the visible viewport
	// return intersection of viewport with elements BCR
	var bcr = elt.getBoundingClientRect(),
		viewport = {x: 0, y: 0, width: window.innerWidth, height: window.innerHeight };
		inter = rect_intersect(bcr, viewport);
	// translate intersection back to elt coordinates
	inter.x -= bcr.x; inter.y -= bcr.y;
	return inter;
}

d3.json("map.json", function (data) {
	var size_name = "icon",
		size = 32,
		border = 10,
		COLUMNS = 20;

	var simpledrag = d3.behavior.drag()
	    .origin(function () {
	        var t = d3.select(this);
	        return {
	            x : parseInt(t.style("left")),
	            y: parseInt(t.style("top"))
	        };
	    })
	    .on("drag", function () {
	    	// console.log("drag");
	        d3.select(this)
	          .style("left", d3.event.x + "px")
	          .style("top", d3.event.y + "px");
	        reframe();
	    });

	var actives = {},
		lastr = {};

	window.setInterval(function () {
		var elt = canvas[0][0],
			visiblerect = visible_rect(elt);

		// console.log(bcr);
		// show the bcr (debug)
		if (lastr.x == visiblerect.x &&
			lastr.y == visiblerect.y &&
			lastr.width == visiblerect.width &&
			lastr.height == visiblerect.height) {
			return;
		}
		lastr = visiblerect;
		/*
		region
			.style("left", visiblerect.x+"px")
			.style("top", visiblerect.y+"px")
			.style("width", (visiblerect.width-18)+"px")
			.style("height", (visiblerect.height-18)+"px")
		*/

		var overlaps = rtree.search({x: visiblerect.x, y: visiblerect.y, w: visiblerect.width, h: visiblerect.height});
		// console.log("overlaps", overlaps[0], "...", overlaps[overlaps.length-1]);
		// console.log("overlaps", overlaps.length);
		// use actives to deactivate...
		for (var i=0, l=overlaps.length; i<l; i++) {
			var elt = overlaps[i],
				d = d3.select(elt).datum,
				base = d.base;
			delete actives[base];
		}
		for (var base in actives) {
			actives[base].classList.remove("active");
		}
		for (var i=0, l=overlaps.length; i<l; i++) {
			var elt = overlaps[i],
				telt = d3.select(elt),
				d = telt.datum,
				base = d.base,
				img = telt.select("img");
			elt.classList.add("active");
			if (img.size() == 0) {
				telt.append("img").attr("src", function (d) {
					return "image1920x/" + d[size_name];
				})
			}
			actives[base] = elt;
		}

	}, 250);

	// function reframe () {
	// 	var elt = canvas[0][0],
	// 		left = -parseInt(elt.style.left),
	// 		w = content_bcr.width,
	// 		top = -parseInt(elt.style.top),
	// 		h = content_bcr.height,
	// 		r = {x: left, y: top, w: w, h: h };

	// 	// for testing //
	// 	// left += 20; right -= 40;


	// 	// console.log("elt", left, content_bcr);
	// }

	var content = d3.select("body")
			.append("div")
			.attr("id", "content"),
		canvas = content
			.append("div")
			.attr("id", "canvas"),
		scale = size/1920,
		content_bcr = content[0][0].getBoundingClientRect();

		/*
	var region = canvas
		.append("div")
		.attr("id", "region");
*/

	canvas.call(simpledrag);

	var rtree = RTree();

	var items = canvas
		.selectAll("div.item")
		.data(data)
		.enter()
		.append("div")
		.attr("class", "item")
		.style("left", function (d, i) {
			var top = (Math.floor(i/COLUMNS)*(size+border));
			var left = ((i%COLUMNS)*(size+border));
			var w = (d.ImageWidth * scale);
			var h = (d.ImageHeight * scale);
			var r = {x: left, y: top, w: w, h: h};
			rtree.insert(r, this);
			// console.log("insert", r, this);
			return left+"px";
		})
		.style("top", function (d, i) {
			var top = (Math.floor(i/COLUMNS)*(size+border));
			return top+"px";
		})
		.style("width", function (d) {
			var w = (d.ImageWidth * scale);
			return w + "px";
		})
		.style("height", function (d) {
			var h = (d.ImageHeight * scale);
			return h + "px";
		});

/* efficient on/off screen events using intervaltree */

})