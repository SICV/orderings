function compare (a, b) {
	if (a<b) return -1;
	if (b<a) return 1;
	else return 0;
}

function compare_reverse (a, b) {
	if (a<b) return 1;
	if (b<a) return -1;
	else return 0;
}

d3.json("index.json", function (data) {
	var key, d, originals = [], orderings = [], images = [];
	var imageWidth, imageHeight;
	var kick_timeout_id,
		walk_across_ordering,
		walk_across_upward = true,
		walk_across_maxsteps = 1,
		walk_across_steps = 0,
		KICKTIME = 15000,
		WALKTIME = 5000;

	for (key in data) {
		// weird this is happening!
		if (key == "null") {
			delete data[key];
		}
		// console.log("key", key, key == "null");
		if (data.hasOwnProperty(key) && (key !== "null")) {
			d = data[key];
			originals.push(key);
			d.original = key;
			images.push(d);
		}
	}
	originals.sort();
	for (key in data[originals[0]]) {
		d = data[originals[0]];
		if (d.hasOwnProperty(key) && key != "original") { orderings.push(key); }
	}
	orderings.sort(layer_sort);
	console.log(originals.length + " originals, " + orderings.length + " orderings");
	console.log("images", images, "orderings", orderings);

	console.log("originals", originals);

	var numorderings = orderings.length,
		numimages = originals.length;
	// data = data.slice(0, 100);
	// calculate the rows
	var rows = [];
	for (var oi=0; oi<numorderings; oi++) {
		var o = orderings[oi];
		var row = {index: oi, ordering: o};
		// added filter because some data seems to be missing (preview)... but why.... MM 14 april
		var cimages = images.filter(function (x) {
			return x[o] !== undefined;
		}).map(function (x) {
			// console.log("x", x);
			x[o].original = x.original;
			return x[o];
		});


		// console.log("cimages", o, cimages);
		cimages.sort(function (a, b) {
			var ac = a["UserComment"],
				bc = b["UserComment"];
			if (ac<bc) return 1;
			if (ac>bc) return -1;
			return compare_reverse(a.original, b.original); 
		});
		row.images = cimages;
		rows.push(row);
	}
	console.log("rows", rows);

	var orderings_window,
		viewersByOrdering = {};
	var viewers = d3.select("#viewer").selectAll("div.viewer").data(orderings);
	viewers.enter()
		.append("div")
		.attr("class", function (d) { return "viewer " + d; })
		.each(function (d) {
			viewersByOrdering[d] = this;
		})
		.append("img")
		.on("load", function () {
			this.parentNode.style.display = "block";
			imageWidth = this.width;
			imageHeight = this.height;
			// console.log("image.load:", this.width, this.height, " window:", window.innerWidth, window.innerHeight);
			update_image_zoom();
			//console.log("image loaded");
			/*
			if (Math.random()*5 < 1) {
				console.log("simulating onload failure!");
				return;
			}
			*/
			walksoon();
		});


	function kicksoon () {
		kick_timeout_id = window.setTimeout(kick, KICKTIME);
	}

	function cancelkick () {
		if (kick_timeout_id != undefined) {
			console.log("cancel kick");
			window.clearTimeout(kick_timeout_id);
			kick_timeout_id = undefined;
		}
	}

	function kick () {
		console.log("KICK!");
		resetimage();
		walksoon();
	}

	function walksoon () {
		console.log("walksoon");
		cancelkick();
		window.setTimeout(walk, WALKTIME);
	}

	// d3.select("#orderings").on("click", function () {
	// 	orderings_window = window.open("orderings.html");
	// });

	window.addEventListener("message", function (event) {
		var m = event.data;
		// if (event.origin !== "http://example.org:8080") return;
		if (m.msg == "itemclick") {
			var d = m.item;
			// console.log("itemclick", d);
			// d3.select("#viewer").text("").append("img").attr("src", d.SourceFile);
			show(d.MakerNote, d.SourceFile);
		} else if (m.msg == "clear") {
			hide(m.ordering);
		} else if (m.msg == "clearall") {
			clearall();
		}
	}, false);

	if (window.opener) {
		window.opener.postMessage({msg: "ready"}, "*");
	}

	function clearall() {
		// console.log("clearall");
		d3.selectAll("div.viewer").style("display", "none");
	}

	function hide(ordering) {
		// console.log("hide", ordering);
		d3.select(viewersByOrdering[ordering]).style("display", "none");
	}

	function show (ordering, src) {
		// console.log("show", ordering, src);
		var v = d3.select(viewersByOrdering[ordering]),
			img = v.select("img");
		v.style("display", "none");
		// var cursrc = img.attr("src");
		// console.log("src", cursrc, src, curdisp);
		if ((ordering == "contours") || (ordering == "gradient")) {
		 	src = src.replace(/\.png$/, ".svg");
		} else if (ordering == "sift") {
		 	src = src + ".svg";
		}
		v.select("img").attr("src", src);
		// console.log(viewersByOrdering[ordering]);
		kicksoon();			
	}

	var viewer = document.getElementById("viewer");
	d3.select("#viewer")
		.on("click", function () {
			// console.log("click", d3.event);
			document.getElementById("zoom").classList.toggle("active");
			update_image_zoom(d3.event.clientX, d3.event.clientY);
		});

	// zoom button
	d3.select("#zoom").on("click", function () {
		this.classList.toggle("active");
		update_image_zoom();
	})

	d3.select("#walk").on("click", function () {
		walk();
	})

	function update_image_zoom (cx, cy) {
		var zoom = (document.getElementById("zoom").classList.contains("active"));
		var useWidth = 1920,
			useHeight = imageHeight * (useWidth / imageWidth);

		if (!zoom) {
			// zoomed out
			// viewer.classList.remove("zoomedin");
			d3.select("#viewer")
				.style("width", "100%")
				.style("height", "100%");
			if ((window.innerWidth * useHeight) / useWidth > window.innerHeight) {
				// use height
				// console.log("zoomedout: using height (portrait)");
				d3.selectAll("img")
					.style("height", "100%")
					.style("width", "auto");
			} else {
				// use width
				// console.log("zoomedout: using width (landscape)");
				d3.selectAll("img")
					.style("width", "100%")
					.style("height", "auto");
			}
		} else {
			// console.log("zoomedin")
			// use cx and cy to match the clicked point
			// rather than window.innerWidth, Height...
			// should use images actual width/height

			// ugh... this is kind of lame here but...
			if (cx !== undefined) {
				var first_visible_image,
					allimages = document.querySelectorAll("img");
				for (var i=0, l=allimages.length; i<l; i++) {
					if (allimages[i].parentNode.style.display == "block") {
						first_visible_image = allimages[i];
						break;
					}
				}
				var image_x = (cx/first_visible_image.width) * useWidth,
					image_y = (cy/first_visible_image.height) * useHeight;
			}
			
			viewer.classList.add("zoomedin");
			d3.select("#viewer")
				.style("width", useWidth+"px")
				.style("height", useHeight+"px");

			if (cx !== undefined) {
				var vf = document.getElementById("viewframe");
				vf.scrollLeft = image_x - cx;
				vf.scrollTop = image_y - cy;				
			}

			d3.selectAll("img")
				.style("width", "100%")
				.style("height", "100%");
		}
		// in all cases reset images to be 100%/100%

	}

	/* images are the objects with keys for each ordering:
		SourceFile, UserComment, ImageWidth, ImageHeight
	*/
	function random_image_index () {
		var i = Math.floor(Math.random()*images.length);
		i = Math.min(images.length-1, Math.max(0, i));
		return i;
	}
	function random_order_for_image (img, starti) {
		// never return the 0 ordering (preview)
		if (starti == undefined) {
			starti = 1 + Math.floor(Math.random()*(orderings.length-1));
		}
		if (starti >= orderings.length) { starti = 1; }
		var i = starti,
			count = 0;
		while (img[orderings[i]].UserComment == 0 || activeLayers[orderings[i]]) {
			console.log("seeking non-zero/non-active ordering");
			if (++i >= orderings.length) {
				i = 1;
			}
			if (++count == orderings.length) {
				console.log("no non-zero ordering found");
				return;
			}
		}
		return orderings[i];
	}
	var current_image,
		current_image_index,
		activeLayersCount = 0,
		activeLayers = {};

	function resetimage() {
		activeLayers = {};
		activeLayersCount = 0;
		current_image = undefined;
	}

	function index_of_image_with_src (arr, sourcefile) {
		for (var i=0, len=arr.length; i<len; i++) {
			if (arr[i].SourceFile == sourcefile) {
				return i;
			}
		}
	}

	function walk () {
		if (walk_across_ordering) {
			var order_index = LAYER_ORDER.indexOf(walk_across_ordering),
				ordered_images = rows[order_index].images,
				current_ordered_index = index_of_image_with_src(ordered_images, current_image[walk_across_ordering].SourceFile);

			// console.log("ordered_images", ordered_images);
			// console.log("current_ordered_index", current_image, current_ordered_index);
			walk_across_steps += 1;
			if ((walk_across_steps <= walk_across_maxsteps) &&
				(walk_across_upward ? (++current_ordered_index < ordered_images.length) : (--current_ordered_index >= 0)) && 
				(ordered_images[current_ordered_index].UserComment !== 0)) {
				// walk to it
				current_image = ordered_images[current_ordered_index];
				// console.log("looking up", current_image.original, "in", data);
				current_image = data[current_image.original];
				console.log("walking across to", current_ordered_index, current_image);
				// current image should have orderings (now it's a specific one)
				show(walk_across_ordering, current_image[walk_across_ordering].SourceFile);
			} else {
				console.log("end of crosswalk");
				walk_across_ordering=undefined;
				walksoon();
				// stop the across_walk
			}
		} else if (current_image == undefined) {
			// start with a random image...
			clearall();
			var rii = random_image_index(),
				ri = images[rii],
				o = random_order_for_image(ri);
			console.log("random_walk", ri, o);
			show(o, ri[o].SourceFile);
			current_image = ri;
			current_image_index = rii;
			activeLayersCount = 1;
			activeLayers[o] = true;
		} else {
			// ... then add another layer
			var o = random_order_for_image(current_image);
			if (o) {
				console.log("selected ordering", o);
				activeLayersCount += 1;
				activeLayers[o] = true;
				// console.log("activeLayersCount", activeLayersCount);
				show(o, current_image[o].SourceFile);
			} else {
				// clearall();
				// // console.log("show", current_image.preview);
				// // show("preview", current_image.preview.SourceFile);
				// resetimage();
				// walksoon();

				// NEW: transition to enter walkAcross
				// reset layers
				activeLayers = {};
				activeLayersCount = 0;
				o = random_order_for_image(current_image);
				walk_across_ordering = o;
				walk_across_upward = (Math.random() >= 0.5);
				walk_across_maxsteps = Math.round(1 + (Math.random() * 5));
				walk_across_steps = 0;
				console.log("entering walkAcross with ordering", walk_across_ordering);
				activeLayers[o] = true;
				activeLayersCount += 1;
				// hide others
				for (var oi=0; oi<orderings.length; oi++) {
					if (orderings[oi] !== o) {
						hide(orderings[oi]);
					}
				}
				walksoon();

			}
		}

	}
	walksoon();
})