# compile meta data
import json

# pn are names that are passed
pn = {}
pn['SourceFile'] = True
pn['Brightness'] = True
pn['UserComment'] = True
pn['MakerNote'] = True
pn['ImageWidth'] = True
pn['ImageHeight'] = True

index = {}

def process_dict (d):
	n = d.get("OriginalRawFileName")
	record = index.get(n)
	if record == None:
		record = {}
		index[n] = record

	anno = {}
	program = None
	for key, value in d.items():
		if key in pn:
			anno[key] = value
		if key == "MakerNote":
			program = value
	if program:
		record[program] = anno

def process_json (d):
	if (type(d) == list):
		for item in d:
			process_json(item)
	else:
		process_dict(d)

from argparse import ArgumentParser
p = ArgumentParser()
p.add_argument("input", nargs="*")
args = p.parse_args()

for n in args.input:
	with open(n) as f:
		data = json.load(f)
		process_json(data)

keys = index.keys()
keys.sort()
output = [index[key] for key in index.keys()]

print json.dumps(index)
