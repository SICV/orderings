import sys

while True:
    line = sys.stdin.readline()
    if line == "":
        break
    if "error" not in line:
        sys.stdout.write(line)
