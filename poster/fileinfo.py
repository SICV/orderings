import sys, csv, os, humanize

""" 

usage: python fileinfo.py dirname [dirname...]

Read all the files of a directory, output filenames, Parent folder(s), size as CSV

"""


dirnames = sys.argv[1:]
if not dirnames:
    print "usage: python fileinfo.py dirname [dirname...]"
    sys.exit(0)

items = {}

for dirname in dirnames:
    dirname = dirname.strip("/")
    _, d = os.path.split(dirname)
    for f in os.listdir(dirname):
        fpath = os.path.join(dirname, f)
        stat = os.stat(fpath)
        if f not in items:
            item = {}
            item['filename'] = f
            item['folders'] = []
            items[f] = item
        else:
            item = items[f]

        item['folders'].append(d)
        if 'size' in item and stat.st_size != item['size']:
            sys.stderr.write("Warning: Mismatching file sizes of duplicate filenames {0} {1} != {2}\n".format(f, stat.st_size, item['size']))
            item['size'] = min(stat.st_size, item['size'])
        else:
            item['size'] = stat.st_size

out = csv.writer(sys.stdout)
keys = items.keys()
keys.sort()
out.writerow(("filename", "folders", "filesize", "filesize h"))
for key in keys:
    item = items[key]
    row = [item['filename'], ",".join(item['folders']), item['size'], humanize.naturalsize(item['size'])]
    out.writerow(row)


