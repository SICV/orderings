import csv, sys, os
from PIL import Image, ImageFont, ImageDraw

csvpath = sys.argv[1]
outpath = sys.argv[2]
firstrow = None
# bgcolor=(128, 164, 128)
# bgcolor=(32, 64, 32) # dark green, sqlid's
# bgcolor=(64, 32, 32) # dark red, sql dimensions
# bgcolor=(32, 32, 64) # dark blue, file size
# bgcolor=(32, 64, 64) # ..., exifdatetimeoriginal
bgcolor=(64, 64, 32) # ..., exif...

forecolor=(255, 255, 255)
fontpath = "/home/murtaugh/.fonts/UniversElse/UniversElse-Bold.ttf"
font = ImageFont.truetype(fontpath, 32)


count = 0

for row in csv.reader(open(csvpath)):
    if not firstrow:
        firstrow = row
    else:
        fname = row[0]
        data = row[1]
        imgpath = "../material.320/{0}".format(fname)
        if os.path.exists(imgpath):
            base, ext = os.path.splitext(fname)
            labelpath = os.path.join(outpath, base+".png")
            thumb = Image.open(imgpath)
            label = Image.new('RGB', thumb.size, bgcolor)
            draw = ImageDraw.Draw(label)
            if " " in data:
                lines = data.split(" ")
                w, h = 0, 0
                lineheight = 0
                for line in lines:
                    ts = font.getsize(line)
                    if ts[0] > w:
                        w = ts[0]
                    h += ts[1]
                    lineheight = ts[1]
                cx = ((thumb.size[0] - w)/2)
                cy = (thumb.size[1] - h)/2
                for line in lines:
                    draw.text((cx, cy), line, font=font, fill=forecolor)
                    cy += lineheight

            else:
                ts = font.getsize(data)
                cx = ((thumb.size[0] - ts[0])/2)
                cy = (thumb.size[1] - ts[1])/2
                draw.text((cx, cy), data, font=font, fill=forecolor)

            f = open(labelpath, "w")
            label.save(f, "png")
            f.close()
            count += 1

