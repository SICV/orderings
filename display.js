d3.json("index.json", function (data) {
	var key, d, originals = [], orderings = [], images = [];
	var imageWidth, imageHeight;

	for (key in data) {
		// weird this is happening!
		if (key == "null") {
			delete data[key];
		}
		// console.log("key", key, key == "null");
		if (data.hasOwnProperty(key) && (key !== "null")) {
			d = data[key];
			originals.push(key);
			d.original = key;
			images.push(d);
		}
	}
	originals.sort();
	for (key in data[originals[0]]) {
		d = data[originals[0]];
		if (d.hasOwnProperty(key) && key != "original") { orderings.push(key); }
	}
	orderings.sort(layer_sort);
	// console.log(originals.length + " originals, " + orderings.length + " orderings");
	// console.log("images", images, "orderings", orderings);

	// console.log("orderings", orderings);

	var numorderings = orderings.length,
		numimages = originals.length;
	// data = data.slice(0, 100);

	var orderings_window,
		viewersByOrdering = {};
	var viewers = d3.select("#viewer").selectAll("div.viewer").data(orderings);
	viewers.enter()
		.append("div")
		.attr("class", function (d) { return "viewer " + d; })
		.each(function (d) {
			viewersByOrdering[d] = this;
		})
		.append("img")
		.on("load", function () {
			this.parentNode.style.display = "block";
			imageWidth = this.width;
			imageHeight = this.height;
			// console.log("image.load:", this.width, this.height, " window:", window.innerWidth, window.innerHeight);
			update_image_zoom();
		});


	// d3.select("#orderings").on("click", function () {
	// 	orderings_window = window.open("orderings.html");
	// });

	window.addEventListener("message", function (event) {
		var m = event.data;
		// if (event.origin !== "http://example.org:8080") return;
		if (m.msg == "itemclick") {
			var d = m.item;
			// console.log("itemclick", d);
			// d3.select("#viewer").text("").append("img").attr("src", d.SourceFile);
			show(d.MakerNote, d.SourceFile);
		} else if (m.msg == "clear") {
			hide(m.ordering);
		} else if (m.msg == "clearall") {
			clearall();
		}
	}, false);

	if (window.opener) {
		window.opener.postMessage({msg: "ready"}, "*");
	}

	function clearall() {
		// console.log("clearall");
		d3.selectAll("div.viewer").style("display", "none");
	}

	function hide(ordering) {
		// console.log("hide", ordering);
		d3.select(viewersByOrdering[ordering]).style("display", "none");
	}

	function show (ordering, src) {
		// console.log("show", ordering, src);
		var v = d3.select(viewersByOrdering[ordering]),
			img = v.select("img");
		v.style("display", "none");
		// var cursrc = img.attr("src");
		// console.log("src", cursrc, src, curdisp);
		if (ordering == "contours") {
		 	src = src.replace(/\.png$/, ".svg");
		} else if (ordering == "gradient") {
		 	src = src.replace(/\.png$/, ".svg");
		} else if (ordering == "sift") {
		 	src = src + ".svg";
		 }
		v.select("img").attr("src", src);
		// console.log(viewersByOrdering[ordering]);
	}

	var viewer = document.getElementById("viewer");
	d3.select("#viewer")
		.on("click", function () {
			// console.log("click", d3.event);
			document.getElementById("zoom").classList.toggle("active");
			update_image_zoom(d3.event.clientX, d3.event.clientY);
		});

	// zoom button
	d3.select("#zoom").on("click", function () {
		this.classList.toggle("active");
		update_image_zoom();
	})

	function update_image_zoom (cx, cy) {
		var zoom = (document.getElementById("zoom").classList.contains("active"));
		var useWidth = 1920,
			useHeight = imageHeight * (useWidth / imageWidth);

		if (!zoom) {
			// zoomed out
			// viewer.classList.remove("zoomedin");
			d3.select("#viewer")
				.style("width", "100%")
				.style("height", "100%");
			if ((window.innerWidth * useHeight) / useWidth > window.innerHeight) {
				// use height
				// console.log("zoomedout: using height (portrait)");
				d3.selectAll("img")
					.style("height", "100%")
					.style("width", "auto");
			} else {
				// use width
				// console.log("zoomedout: using width (landscape)");
				d3.selectAll("img")
					.style("width", "100%")
					.style("height", "auto");
			}
		} else {
			// console.log("zoomedin")
			// use cx and cy to match the clicked point
			// rather than window.innerWidth, Height...
			// should use images actual width/height

			// ugh... this is kind of lame here but...
			if (cx !== undefined) {
				var first_visible_image,
					allimages = document.querySelectorAll("img");
				for (var i=0, l=allimages.length; i<l; i++) {
					if (allimages[i].parentNode.style.display == "block") {
						first_visible_image = allimages[i];
						break;
					}
				}
				var image_x = (cx/first_visible_image.width) * useWidth,
					image_y = (cy/first_visible_image.height) * useHeight;
			}
			
			viewer.classList.add("zoomedin");
			d3.select("#viewer")
				.style("width", useWidth+"px")
				.style("height", useHeight+"px");

			if (cx !== undefined) {
				var vf = document.getElementById("viewframe");
				vf.scrollLeft = image_x - cx;
				vf.scrollTop = image_y - cy;				
			}

			d3.selectAll("img")
				.style("width", "100%")
				.style("height", "100%");
		}
		// in all cases reset images to be 100%/100%

	}

})