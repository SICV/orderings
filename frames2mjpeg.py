from PIL import Image
import sys
from argparse import ArgumentParser
from cStringIO import StringIO
from math import floor, ceil

p = ArgumentParser("")
p.add_argument("--limit", type=int, default=None)
p.add_argument("--skip", type=int, default=None)
p.add_argument("--width", type=int, default=640)
p.add_argument("--height", type=int, default=480)
p.add_argument("--fps", type=int, default=10)
p.add_argument("--background", default="black")
p.add_argument("--output", default="output.mjpeg")
p.add_argument("input", default=[], nargs="*")
args = p.parse_args()

with open (args.output, "w") as f:
    W,H = (args.width, args.height)
    numimgs = len(args.input)
    if args.limit:
        numimgs = min(numimgs, args.limit)

    for i, p in enumerate(args.input):
        if args.skip and i<args.skip:
            continue
        p = p.decode("utf-8")
        perc = (float(i) / numimgs)
        percentage = int(floor(perc*100))
        bars = int(ceil(perc*20))
        bar = ("*"*bars) + ("-"*(20-bars))
        msg = u"\r{0} {1}/{2} {3}... ".format(bar, (i+1), numimgs, p)
        sys.stderr.write(msg.encode("utf-8"))
        sys.stderr.flush()
        im = Image.open(p)
        im.thumbnail((W,H))
        w, h = im.size
        ix = (W-w)/2
        iy = (H-h)/2
        bim = Image.new("RGB", (W,H), color=args.background)
        bim.paste(im, (ix, iy))
        # somewhat confusingly, this doesn't seem to work...
        # bim.save(f, format="jpeg")

        # so using a stringio
        out = StringIO()
        bim.save(out, format="jpeg")
        f.write(out.getvalue())

        if args.limit and i+1 >= args.limit:
            break

sys.stderr.write("wrote output to {0}\n".format(args.output))


# ffmpeg -framerate 10 -i output.mjpeg output.webm
#sys.stderr.write("using ffmpeg to compress...\n")
#ffmpeg(("-framerate", str(args.fps), "-i", args.output, args.output+".webm"))
#sys.stderr.write("done\n")
