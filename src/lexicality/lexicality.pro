TEMPLATE = app
CONFIG += console
CONFIG += qt

SOURCES += main.cpp

unix:!macx:!symbian: LIBS += -ltesseract
unix:!macx:!symbian: LIBS += -llept

CONFIG +=link_pkgconfig
PKGCONFIG += opencv
