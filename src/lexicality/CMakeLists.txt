cmake_minimum_required (VERSION 2.6)
project (lexicality)

# Add the including directory of the tesseract 
# and please replace with your dir.
#include_directories (/home/ytxie/include)
# Add the search directory for the tesseract library 
# and please replace with your dir.
#link_directories (/home/ytxie/lib)

find_package ( OpenCV REQUIRED )

# QT5
find_package(Qt5Widgets)
find_package(Qt5Declarative)

add_executable (lexicality main.cpp)

# link the leptonica library and the tesseract library
target_link_libraries (lexicality lept tesseract ${OpenCV_LIBS} ${Qt5Widgets_LIBRARIES} )