#!/usr/bin/env python
#-*- coding:utf-8 -*-

import pyexiv2, sys
import json

data = []
# sort by datetime
# question of orders -- does the scanning order match the tagging order?
# how to 
for p in sys.argv[1:]:
    try:
        meta = pyexiv2.ImageMetadata(p)
        meta.read()
        # print meta.exif_keys
        m = meta['Exif.Photo.DateTimeOriginal']
        # print m, dir(m), m.value
        data.append({
            'filename': p,
            'exif.photo.datetimeoriginal': meta['Exif.Photo.DateTimeOriginal'].raw_value,
            'exif.image.make': meta['Exif.Image.Make'].raw_value,
            'exif.image.model': meta['Exif.Image.Model'].raw_value
        })
    except KeyError:
        sys.stderr.write("Tag Missing in: {0}\n".format(p))
        pass

print json.dumps(data)



