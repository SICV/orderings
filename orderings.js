(function () {

var display;
var key, d, originals, orderings, images;
var active_original;
var show_data;

var LEFT_BORDER = 200;

function compare (a, b) {
	if (a<b) return -1;
	if (b<a) return 1;
	else return 0;
}

function compare_reverse (a, b) {
	if (a<b) return 1;
	if (b<a) return -1;
	else return 0;
}

function hide (d) {
	// console.log("hide", d);
	display.postMessage({msg: "clear", ordering: d.MakerNote}, "*");
	d3.select("#content")
		.select("div."+d.MakerNote)
		.selectAll("img")
		.classed("active", false);
}

function show (d) {
	var autoclear = !(document.getElementById("mix").classList.contains("active"));
	// console.log("autoclear", autoclear);
	if (autoclear && ((active_original !== undefined) && (d.original != active_original))) {
		// AUTOCLEAR
		// console.log("autoclear");
		display.postMessage({msg: "clearall"}, "*");
		d3.select("#content").selectAll("img").classed("active", false);
	}
	active_original = d.original;
	// reset active img in row
	d3.select("#content")
		.select("div."+d.MakerNote)
		.selectAll("img")
		.classed("active", function (d2) { return (d2 == d ) });

	if (display == undefined || display.closed) {
		show_data = d;
		display = window.open("display.html", "display");
		// wait for it...
	} else {
		display.postMessage({msg: "itemclick", item: d}, "*");
	}
}

window.addEventListener("message", function (event) {
	var m = event.data;
	// if (event.origin !== "http://example.org:8080") return;
	if (m.msg == "ready" && show_data !== undefined) {
		display.postMessage({msg: "itemclick", item: show_data}, "*");
	}
}, false);

d3.json("index.json", function (data) {
	restart(data);
})

function scrollLeftTween(scrollLeft) { 
    return function() { 
        var i = d3.interpolateNumber(this.scrollLeft, scrollLeft); 
        return function(t) { this.scrollLeft = i(t); }; 
    }; 
}

function status (txt) {
	d3.select("#overlay").select(".status").select(".contents").text(txt);
}

// mix button
d3.select("#mix").on("click", function () {
	this.classList.toggle("active");
})

d3.select("#reloadbutton").on("click", function () {
	status("reloading...");
	d3.select("#overlay").classed("busy", true);
	d3.json("index.json", function (data) {
		console.log("reload", data);
		restart(data);
		status("done.");
		d3.select("#overlay").classed("busy", false);
		window.setTimeout(function () {
			status("");
		}, 3000);
	});
});

/*
d3.select("#scanbutton").on("click", function () {
	// window.location.reload(false); 
	// return;
	var the_button = d3.select(this);
	if (the_button.text() == "reload") {
		window.location.reload(false);
		return;
	}

	d3.select("#overlay").classed("busy", true);
	the_button.attr("disabled", "disabled");
	status("scanning...");
	d3.json("/cgi-bin/make.cgi?f=scan", function (data) {
		// console.log("postscan", data);
		status("Processing... This takes approximately 4 minutes (you can continue to browse in this time).");
		d3.json("/cgi-bin/make.cgi?f=all", function (data) {
			// console.log("postmake", data);
			status("Ready. Click on Reload to see the results");
			// window.location.reload(false);
			the_button
				.text("reload")
				.attr("disabled", null);
			d3.json("/cgi-bin/make.cgi?f=all", function (data) {});
			// d3.json("index.json", function (data) {
			// 	console.log("reload", data);
			// 	restart(data);
			// 	status("done.");
			// 	the_button.attr("disabled", null);
			// 	d3.select("#overlay").classed("busy", false);
			// 	window.setTimeout(function () {
			// 		status("");
			// 	}, 3000);
			// });
		});

	})
}).attr("disabled", null);
*/

function restart (data) {
	var key, d;	
	originals = [];
	orderings = [];
	images =[];
	for (key in data) {
		// weird this is happening!
		if (key == "null") {
			delete data[key];
		}
		// console.log("key", key, key == "null");
		if (data.hasOwnProperty(key) && (key !== "null")) {
			d = data[key];
			originals.push(key);
			d.original = key;
			images.push(d);
		}
	}
	originals.sort();
	// refresh orderings list
	for (key in data[originals[0]]) {
		d = data[originals[0]];
		if (d.hasOwnProperty(key) && key != "original") { orderings.push(key); }
	}
	orderings.sort(layer_sort);
	// console.log(originals.length + " originals, " + orderings.length + " orderings");
	// console.log("images", images, "orderings", orderings);
	var numorderings = orderings.length,
		numimages = originals.length;
	// data = data.slice(0, 100);
	var rows = [];
	for (var oi=0; oi<numorderings; oi++) {
		var o = orderings[oi];
		var row = {index: oi, ordering: o};
		// added filter because some data seems to be missing (preview)... but why.... MM 14 april
		var cimages = images.filter(function (x) {
			return x[o] !== undefined;
		}).map(function (x) {
			// console.log("x", x);
			x[o].original = x.original;
			return x[o];
		});


		// console.log("cimages", o, cimages);
		cimages.sort(function (a, b) {
			var ac = a["UserComment"],
				bc = b["UserComment"];
			if (ac<bc) return 1;
			if (ac>bc) return -1;
			return compare_reverse(a.original, b.original); 
		});
		row.images = cimages;
		rows.push(row);
	}
	var content = d3.select("#content"),
		row = content.selectAll("div.row").data(rows)
			.enter()
			.append("div")
			.attr("class", function (d) { return "row "+d.ordering });
	row.append("div").attr("class", "label").text(function (d) { return d.ordering });
	var update = row
		.append("div").attr("class", "scroll")
		.append("div").attr("class", "content")
		.selectAll("img")
		.data(function (d) { return d.images }, function (d) { return d.original });

	update.enter()
		.append("div")
		.attr("class", "img")
		.style("left", function (d, i) {
			return (LEFT_BORDER + (i * 32)) + "px";
		})
		.attr("title", function (d) {
			if (d.MakerNote == "preview") {
				return d.original;
			} else {
				return d.original + " (" + d.UserComment + ")";
			}
		})
		.attr("data-src", function (d) { return d.SourceFile.replace(/\.png$/, ".icon.png") })
		.on("click", function (d) {
			var prow = d3.select(this.parentNode).datum();
			// console.log("d", prow, d);
			// locate image in all other rows, scroll to match position
			// get relative position of the clicked element

			var tc = d3.select(this).select("img").attr("class");
			if (tc == null || tc.indexOf("active") == -1) {
				show(d);
			} else {
				hide(d);
			}
			// t.classed("active", true);

			var s = this.parentNode.parentNode,
				dx = (s.scrollLeft - this.offsetLeft);
			var items = content.selectAll("div.img")
				.filter(function (d2) {
					return (d2.original == d.original)
				}).each(function (d2) {
					var s = this.parentNode.parentNode,
						left = this.offsetLeft;
					// s.scrollLeft = left + dx;
					d3.select(s).transition().duration(1000).tween("scrollLeft", scrollLeftTween (left+dx));
					// if (d2 == d) {
					// 	console.log(this, s.scrollLeft, s.scrollLeftMax);
					// }
				});

			// console.log("items", items[0]);
		});

	update.order();

	row.on("mouseover", function () {
		d3.select(this).select(".scroll").style("overflow-x", "auto");
	}).on("mouseout", function () {
		d3.select(this).select(".scroll").style("overflow-x", "hidden");
	}).each(function () {
		d3.select(this).select(".scroll").each(function () { this.scrollLeft = 0; });	
	});

	// adjust row sizes
	var content = d3.selectAll("div.content")
		.style("width", function (d) {
			return (LEFT_BORDER + (d.images.length * 32)) + "px";
		});

	// revealevents
	window.setTimeout (function () {
		var contentdivs = Array.prototype.slice.call(document.querySelectorAll("div.content"));
		contentdivs.forEach(function (cdiv) {
			// console.log("cd", cdiv);
			var reveal = revealevents(cdiv, {
				elt_id: function () {
					return this.getAttribute("data-src");
				},
				reveal: function () {
					// console.log("enter", this);
					var telt = d3.select(this),
						src = this.getAttribute("data-src"),
						img = telt.select("img");
					if (img.size() == 0) {
						telt.append("img").attr("src", src);
						reveal.remove(this);
					}
				}
			});
			reveal.add("div.img")
		})

	}, 10);

}


})();
